# Trabbi: a suite of programs for static web-site authoring

Trabbi includes a set of simple CLI programs that can be used together to help
make the process of maintaining a static web-site simple, easy, flexible, and
fast.

Most of these tools use stdin or JSON/text files to specify/create data. So
you can probably create your own scripts in any language and combine them with
trabbi tools to roll out your own toolchain.

Simplicity? I'm in no position to judge.

## Why "Trabbi"?

In pre-reunification [DDR], a "[Trabbi]" was a state produced car. If the 
[jokes] are anything to go by, it takes about eight years for one of them to be
manufactured once an order is given. This draws a parallel to my implementation
of this project; superflous planning, countless deleted repos, and barely
finished source code.

Also, I had a (good enough) Python version of one of the programs, but scrapped
it to start a Golang implementation. [OCD] sucks.

[Soviet Union]: http://en.wikipedia.org/wiki/Soviet_Union
[OCD]: http://en.wikipedia.org/wiki/Obsessive_Compulsive_Disorder
[DDR]: http://en.wikipedia.org/wiki/German_Democratic_Republic
[Trabbi]: http://en.wikipedia.org/wiki/Trabant
[jokes]: https://en.wikipedia.org/wiki/East_Germany_jokes#Trabant
[Lenin]: http://en.wikipedia.org/wiki/Vladimir_Lenin
[Stalin]: http://en.wikipedia.org/wiki/Josef_Stalin
