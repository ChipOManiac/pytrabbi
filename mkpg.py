#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

# > imports
import sys
import getopt
from time import gmtime, strftime
from Trabbi import ContentFunc as cf
from Trabbi import DataFunc as df
from Trabbi import OutFunc as otfn

# > MAIN FUNCTION
def main():
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "s:d:m:t:o:T:",
            ["source-file", "data-files", "md-link-files", "template-file", "output-file", "time-stamp-format"]
        )
    except getopt.GetOptError as err:
        print(err)
        sys.exit(2)

    # storage variables
    arg_source_file = None
    arg_data_file_list = list()
    arg_md_link_file_list = list()
    arg_template_file = None
    arg_output_file = None
    arg_time_stamp_format = "%Y-%m-%d %H:%M:%S"

    for o, a in opts:
        if o in ("-s", "--source-file"):
            arg_source_file = a
        elif o in ("-d", "--data-files"):
            arg_data_file_list = a.split(',')
        elif o in ("-m", "--md-link-files"):
            arg_md_link_file_list = a.split(',')
        elif o in ("-t", "--template-file"):
            arg_template_file = a
        elif o in ("-o", "--output-file"):
            arg_output_file = a
        elif o in ("-T", "--time-stamp-format"):
            arg_time_stamp_format = a
        else:
            assert False, "option not implemented"

    # check essential files
    if None in (arg_source_file, arg_template_file, arg_output_file):
        print("Essential argument (source, template or output) omitted!")
        sys.exit(2)

    # procedure
    # WARNING: UGLY AS HELL!
    header_json_string, content_string = cf.split_source_file(arg_source_file)

    # - data files from the arguments
    data_string_list = list()
    for data_file in arg_data_file_list:
        data_string_list.append(df.get_data_file_string(data_file))
    data_string_list.append(header_json_string) # add data string from content

    # - process data list
    merged_dict = df.merge_data_dicts(data_string_list)

    # - md links
    md_link_string_list = list()
    for md_link_file in arg_md_link_file_list:
        md_link_string_list.append(cf.get_md_link_file_string(md_link_file))

    joined_md_links = cf.join_md_link_strings(md_link_string_list)

    # - create content string
    full_content_string = cf.create_full_content_string(content_string, joined_md_links)

    # - render markdown
    html_content_string = otfn.render_content_string(full_content_string)

    # - add content string to merged_dict
    merged_dict["content"] = html_content_string

    # - add timestamp to merged_dict
    merged_dict["generation_time_stamp"] = strftime(arg_time_stamp_format, gmtime())
    
    # - render complete HTML
    html_page_string = otfn.render_output_page(merged_dict, arg_template_file)

    # - write to file
    with open(arg_output_file, 'w') as flhd:
        flhd.write(html_page_string)

# > MAIN
if __name__ == "__main__":
    main()
