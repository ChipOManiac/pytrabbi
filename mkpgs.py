#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

# > imports
import sys
import json
from Trabbi import ContentFunc as cnfn
from Trabbi import DataFunc as dtfn
from Trabbi import OutFunc as otfn

# > visualization of json
# {
#   "source-dir": "/home/foo/source/",
#   "data-dir": "/home/foo/data/",
#   "output-dir": "/var/www/html/",
#   "time-stamp-format": "%X%X%X %G%G%G",
#   "data-files": [ "data1.json", "data2.json", "/var/log/foo.json" ],
#   "link-files": [ "link1.md", "link2.md", "/var/log/foo.md" ],
#   "pages": [
#       { "source-file": "src1.md", "output-file": "out1.md" },
#       { "source=file": "src2.md", "output-file": "out2.md" }
#   ]
# }

# > functions

