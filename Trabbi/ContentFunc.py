#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

# > functions
# content file splitter
def split_source_file(source_file_path):
    with open(source_file_path, 'r') as flhd:
        header_string, content_string = flhd.read().split("~~~~~~~")

    header_json_string = '{ ' + header_string + ' }'

    return header_json_string, content_string

# get link file string
def get_md_link_file_string(md_link_file_path):
    with open(md_link_file_path, 'r') as flhd:
        md_link_file_string = flhd.read()

    return md_link_file_string

# join links together
def join_md_link_strings(md_link_string_list):
    temp_string = ''

    for md_link_string in md_link_string_list:
        temp_string = temp_string + md_link_string

    return temp_string

# create joined content string
def create_full_content_string(content_string, md_link_string):
    if md_link_string != '':
        return content_string + '\n\n' + md_link_string
    else:
        return content_string
