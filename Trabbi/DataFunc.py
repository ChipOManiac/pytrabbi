#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

# > import
import json

# data file string getter
def get_data_file_string(data_file):
    with open(data_file, 'r') as flhd:
        data_file_string = flhd.read()

    return data_file_string

# merge data dicts together to create a big ol' one.
def merge_data_dicts(data_dict_string_list):
    temp_dict = dict()

    for data_dict_string in data_dict_string_list:
        temp_dict.update(json.loads(data_dict_string))

    return temp_dict
