#!/usr/bin/env python3
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

# > imports
import markdown
import pystache

# > functions

# render content string
def render_content_string(content_string):
    rendered_content_string = markdown.markdown(content_string)

    return rendered_content_string

# render page
def render_output_page(data_dict, template_file):
    with open(template_file, 'r') as flhd:
        template_string = flhd.read()

    output_html_string = pystache.render(template_string, data_dict)

    return output_html_string

# output to file
def write_to_file(html_string, output_file):
    with open(output_file, 'w') as flhd:
        flhd.write(html_string)
